import cv2 as cv
import numpy as np

cap = cv.VideoCapture(0)

while True:

	_,frame = cap.read()
	frame = cv.flip(frame,1)
	roi = frame[20:320,0:320]
	gray = cv.cvtColor(roi,cv.COLOR_BGR2GRAY)
	blur = cv.GaussianBlur(gray,(5,5),0)
	ret,thresh = cv.threshold(blur,127,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
	im,contours,hierarchy = cv.findContours(thresh,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
	cnt = contours[0]
	cv.drawContours(frame,[cnt],0,(0,255,0),3)
	area = cv.contourArea(cnt)
	peri = cv.arcLength(cnt,True)
	print(peri)
	if area > 70000 or peri < 2000:
		cv.putText(frame,'fist',(480,300),cv.FONT_HERSHEY_SIMPLEX,2,(255,255,255),2,cv.LINE_AA)
	else:
		cv.putText(frame,'open',(480,300),cv.FONT_HERSHEY_SIMPLEX,2,(255,255,255),2,cv.LINE_AA)
	
	cv.rectangle(frame,(0,20),(320,320),(0,255,0),3)
	cv.imshow('result',frame)
	cv.imshow('thresh',thresh)

	if cv.waitKey(1) & 0xFF == 13:
		break
cap.release()
cv.destroyAllWindows()
