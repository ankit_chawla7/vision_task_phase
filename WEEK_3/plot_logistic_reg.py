#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 18:08:42 2019

@author: blackfly
"""

import pandas as pd

import matplotlib.pyplot as plt

data = pd.read_csv("ex2data1.txt",header = None)
X = data.iloc[:,0:2]
y = data.iloc[:,2]
mask = y == 1
#print(X[mask][0])
adm = plt.scatter(X[mask][0], X[mask][1],c = 'black',marker = '+')
notadm = plt.scatter(X[~mask][0], X[~mask][1],c = 'yellow')
plt.xlabel("Exam 1 Score")
plt.ylabel("Exam 2 Score")
plt.legend((adm,notadm),('Admitted','Not Admitted'))
plt.show()
