import numpy as np
import cv2 as cv
from sklearn.cluster import KMeans

image = cv.imread('blue.jpeg',1)
img_reshape = image.reshape((image.shape[0]*image.shape[1],3))

cluster = KMeans(n_clusters = 2).fit(img_reshape)
rgb = cluster.cluster_centers_
np.round(rgb)

dominant_color = np.zeros(image.shape,dtype = np.uint8)
sec_dominant_color = np.zeros(image.shape,dtype = np.uint8)
dominant_color[:,:,:] = rgb[0,:]
sec_dominant_color[:,:,:] = rgb[1,:]

#swapping
labels = cluster.labels_
labels = labels.reshape((image.shape[0],image.shape[1]))

for i in range(image.shape[2]):
	image[:,:,i] = image[:,:,i]*labels

dcolor = np.where((labels == 0))
sdcolor = np.where((labels == 1))
image[dcolor] = rgb[1,:]
image[sdcolor] = rgb[0,:]

cv.imshow('image',image)
cv.imshow('dominant color',dominant_color)
cv.imshow('second dominant color',sec_dominant_color)
cv.waitKey()
cv.destroyAllWindows()
